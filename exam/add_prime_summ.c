#include <unistd.h>

int ft_putnbr(int i)
{
	char c;

	if (i > 2147483647)
	{
		write(1, "0\n", 2);
		return (0);
	}
	if (i > 9 && i < 2147483647)
	{
		ft_putnbr(i / 10);
		ft_putnbr(i % 10);
	}
	else
	{
		c = i + '0';
		write (1, &c, 1);
	}
	return (0);
}

int ft_check_prime(int i)
{
	int x;

	x = 2;
	while (x <= i)
	{
		if (i % x == 0 && i != x)
			return (0);
		x++;
	}
	return (i);
}

int ft_prime_sum(int i)
{
	int x;
	int result;

	x = 2;
	result = 0;
	while (x <= i)
	{
		if (ft_check_prime(x) != 0)
			result = result + x;
		x++;
	}
	return (result);
}

int ft_atoi(char *av)
{
	int r;

	r = 0;
	if (*av == '+')
		av++;
	if (*av == '-')
		return (0);
	while (*av >= 48 && *av <= 57)
	{
		if (r != (r * 10) / 10)
			return (0);
		r = r * 10;
		if (r != r + (*av - '0') - (*av - '0'))
			return (0);
		r = r + *av - '0';
		av++;
	}
	return (r);
}

int main(int ac, char **av)
{
    int i;

    if (ac != 2)
    {
        write(1, "0\n", 2);
        return (0);
    }
    else
    {
        if ((i = ft_atoi(av[1])) == 0)
		{
			write(1, "0\n", 2);
        	return (0);
		}
		i = ft_prime_sum(i);
		ft_putnbr(i);
		write(1, "\n", 1);
    }
    return (0);
}
/* max size 225286 */