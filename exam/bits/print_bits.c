#include <unistd.h>

void print_bits(unsigned char c)
{
    int i = 7;

    while (i != -1)
    {
        (c & (1 << i)) == (1 << i) ? write(1, "1", 1) : write(1, "0", 1);
        i--;   
    }
    write(1, "\n", 1);
}

int main()
{
    unsigned char c = 8;
    print_bits(c);
    return (0);
}