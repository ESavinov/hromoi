#include <stdio.h>
#include <unistd.h>

void print_bits(unsigned char c)
{
    int i = 7;
    while (i != -1)
    {
        if ((c & (1 << i)) == (1 << i))
            write(1, "1", 1);
        else
            write(1, "0", 1);
        i--;   
    }
    write(1, "\n", 1);
}

unsigned char reverse_bits(unsigned char c)
{
    int k = 7;
    int z = 1;
    int i = 0;
    unsigned char b = 0;
    unsigned char r;

    while (i != 8)
    {
        if(i <= 3)
        {
            r = ((c & (1 << i)) << k);
            k -= 2;
        }
        else
        {
            r = ((c & (1 << i)) >> z);
            z += 2;
        }
        b = b | r;
        i++;
    }
    return (b);
}

int main()
{
    unsigned char c = 64;
    printf("%d\n", c);
    c = reverse_bits(c);
    printf("%d\n", c);
    return (0);
}