#include <stdio.h>

unsigned char swap_bits(unsigned char c)
{
    c = ((c >> 4) | (c << 4));
    return (c);
}

int main()
{
    unsigned char c = 111;
    printf("%d\n", c);
    c = swap_bits(c);
    printf("%d\n", c);
    return (0);
}
