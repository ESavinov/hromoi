#include <unistd.h>

void    brainfuck(char *av)
{
    int     i = 2048;
    int     j = 0;
    char    t[i];
    int     count = 0;

    while (j <= i)
    {
        t[j] = '\0';
        j++;
    }
    i = 0;
    while (*av)
    {
        if (*av == '>')
            i++;
        else if (*av == '<')
            i--;
        else if (*av == '+')
            t[i] += 1;
        else if (*av == '-')
            t[i] -= 1;
        else if (*av == '.')
            write (1, &t[i], 1);
        else if (*av == '[' && t[i] == 0)
        {
            count = 1;
            av++;
            while (count)
            {
                if (*av == '[' && *av)
                    count++;
                if (*av == ']' && *av)
                    count--;
                if (count)
                    av++;
            }
        }
        else if (*av == ']' && t[i] != 0)
        {
            count = 1;
            av--;
            while(count)
            {
                if (*av == ']' && *av)
                    count++;
                if (*av == '[' && *av)
                    count--;
                if (count)
                    av--;
            }
        }
        av++;
    }
}

int     main(int ac, char **av)
{
    if (ac == 2)
        brainfuck(av[1]);
    else
        write(1, "\n", 1);
    return (0);
}