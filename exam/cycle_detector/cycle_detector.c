#include "list.h"

int cycle_detector(const t_list *temp)
{
	t_list *cycle1;
	t_list *cycle2;

	cycle1 = (t_list *)temp;
	cycle2 = (t_list *)temp;
	while (cycle2)
	{
		if (cycle && cycle1->next)
			cycle1 = cycle1->next->next;
		else
			return (0);
		cycle2 = cycle2->next;
		if (cycle1 == cycle2)
			return (1);
	}
	return (0);
}

int main(void)
{
	t_list lst;
	t_list lst1;
	t_list lst2;
	t_list lst3;
	t_list lst4;
	t_list lst5;
	
	lst.next = &lst1;
	lst1.next = &lst2;
	lst2.next = &lst3;
	lst3.next = &lst4;
	lst4.next = &lst5;
	lst5.next = NULL;
	printf("%d", cycle_detector(&lst));
	return (0);
}
