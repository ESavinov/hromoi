#ifndef LIST_H
#define LIST_H

#include <stdio.h>

typedef struct	s_list
{
	struct s_list	*next;
	int				data;
}				t_list;

int cycle_detector(const t_list *temp);

#endif
