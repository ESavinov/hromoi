#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char **argv)
{
	int i;
	if(argc == 4)
	{
		int a;
		int b;
		int c;

		i = 0;
		a = atoi(argv[1]);
		b = atoi(argv[3]);
		while(argv[2][i] != '\0')
			i++;
		i--;
		if(i == 0)
		{
			if(argv[2][i] == '*')
			{
				c = a * b;
				printf("%d\n", c);
			}
			else if(argv[2][i] == '/')
			{
				c = a / b;
				printf("%d\n", c);
			}
			else if(argv[2][i] == '-')
			{
				c = a - b;
				printf("%d\n", c);
			}
			else if(argv[2][i] == '+')
			{
				c = a + b;
				printf("%d\n", c);
			}
			else if(argv[2][i] == '%')
			{
				c = a % b;
				printf("%d\n", c);
			}
		}
	}
	return (0);
}
