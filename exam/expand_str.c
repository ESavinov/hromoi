#include <unistd.h>

void	expand_str(char *av)
{
	while (*av)
	{
		while (*av && (*av == 9 || *av == 32))
			av++;
		while (*av && *av != 9 && *av != 32)
			write(1, av++, 1);
		while (*av && (*av == 9 || *av == 32))
			av++;
		if (*av)
			write(1, "   ", 3);
	}
	write(1, "\n", 1);
}

int		main(int ac, char **av)
{
	if (ac == 2)
		expand_str(av[1]);
	else
		write(1, "\n", 1);
	return (0);
}
