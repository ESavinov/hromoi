#include <stdio.h>
#include <stdlib.h>
#include "flood_fill.h"

char	**make_zone(char **zone)
{
	int i = 0;
	int j = 0;
	int k = 5;
	int l = 8;
	char **temp;

	temp = (char **)malloc((k + 1) * sizeof(char *));
	while (zone[i])
	{
		temp[i] = (char *)malloc((l + 1) * sizeof(char));
		while (zone[i][j])
		{
			temp[i][j] = zone[i][j];
			j++;
		}
		temp[i][j] = '\0';
		j = 0;
		i++;
	}
	temp[i] = NULL;
	return (temp);
}

void	print_map(char **zone)
{	
	int	i = 0;
	int	j = 0;

	while (zone[j])
	{
		while(zone[j][i])
		{
			printf("%c", zone[j][i]);
			i++;
		}
		printf("%c", '\n');
		i = 0;
		j++;
	}
	printf("%c", '\n');
}

void	check_flood_fill(char **area, int x, int y, int prev_color, t_point size)
{
	int max_weight = size.y;
	int max_height = size.x;

	if (x < 0 || y < 0 || x >= max_weight || y >= max_height)
		return ;
	if (area[x][y] != prev_color)
		return ;
	area[x][y] = 'F';
	check_flood_fill(area, x + 1, y, prev_color, size);
	check_flood_fill(area, x - 1, y, prev_color, size);
	check_flood_fill(area, x, y + 1, prev_color, size);
	check_flood_fill(area, x, y - 1, prev_color, size);
	// check_flood_fill(area, x + 1, y + 1, prev_color, size);
	// check_flood_fill(area, x - 1, y + 1, prev_color, size);
	// check_flood_fill(area, x - 1 , y - 1, prev_color, size);
	// check_flood_fill(area, x + 1, y - 1, prev_color, size);
}

void	flood_fill(char **area, t_point size, t_point begin)
{
	int x = begin.x;
	int y = begin.y;
	int prev_color;

	prev_color = area[x][y];
	check_flood_fill(area, x, y, prev_color, size);
}

int	main()
{
	char	**area;
	t_point size = { 8, 5 };
	t_point begin = { 2, 2 };

	char *zone[] = {
        "11111111",
        "10001001",
        "10010001",
        "10110001",
        "11100001"
    };
	print_map(zone);
	area = make_zone(zone);
	flood_fill(area, size, begin);
	print_map(area);
	return (0);	
}