#include "flood_fill.h"

void	check_flood_fill(char **area, int x, int y, int prev_color, t_point size)
{
	int max_weight = size.y;
	int max_height = size.x;

	if (x < 0 || y < 0 || x >= max_weight || y >= max_height)
		return ;
	if (area[x][y] != prev_color)
		return ;
	area[x][y] = 'F';
	check_flood_fill(area, x + 1, y, prev_color, size);
	check_flood_fill(area, x - 1, y, prev_color, size);
	check_flood_fill(area, x, y + 1, prev_color, size);
	check_flood_fill(area, x, y - 1, prev_color, size);
}

void	flood_fill(char **area, t_point size, t_point begin)
{
	int x = begin.y;
	int y = begin.x;
	int prev_color;

	prev_color = area[x][y];
	check_flood_fill(area, x, y, prev_color, size);
}
