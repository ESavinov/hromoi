#include <stdlib.h>
#include <stdio.h>

void ft_fprime(int i)
{
	int j;

	j = 2;
	if (i == 1)
		printf("%d\n", 1);
	while (j != i && i != 1)
	{
		if (i % j == 0)
		{
			printf("%d*", j);
			i = i / j;
		}
		else
		{
			j++;
			if (i == j)
				printf("%d\n", j);
		}
	}
}

int main(int ac, char **av)
{
	int i;

	if (ac == 2)
	{
		if ((i = atoi(av[1])) < 0)
			printf("%c", '\n');
		else
			ft_fprime(i);
	}
	else
		printf("%c", '\n');
	return (0);
}
