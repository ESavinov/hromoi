#include <stdlib.h>
#include <stdio.h>

int fprime(int res)
{
	int i = 2;

	while (res && res != 1)
	{
		if (res % i == 0)
		{
			res /= i;
			if (res != 1)
				printf("%d*", i);
			if (res == 1)
			{
				printf("%d\n", i);
				return (0);
			}
		}
		else
			i++;
	}
	printf("%d\n", res);
	return (0);
}

int main(int ac, char **av)
{
	if (ac == 2)
		fprime(atoi(av[1]));
	else
		printf("\n");
	ac = 0;
	return (0);
}
