/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 12:04:33 by yperra-f          #+#    #+#             */
/*   Updated: 2018/12/06 12:21:26 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 12:04:31 by yperra-f          #+#    #+#             */
/*   Updated: 2018/12/06 12:04:31 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <unistd.h>
#include <stdio.h>

int ft_atoi(const char *str)
{
	int i;
	int sign;
	int r;

	sign = 1;
	r = 0;
	i = 0;	
	while ((str[i] == 9 || str[i] == 10 || str[i] == 11
				|| str[i] == 12 || str[i] == 13 || str[i] == ' ') && str[i] != '\0')
		i++;
	if (str[i] == '-')
	{
		sign = -1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while(str[i] >= 48 && str[i] <= 57)
	{
		r = r * 10 + str[i] - '0';
		i++;
	}
	return (r * sign);
}

int main()
{
	char str [] = "   -23;odkfok234";
	printf("%d", ft_atoi(str));
	return (0);
}
