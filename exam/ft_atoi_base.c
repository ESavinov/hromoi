#include <stdio.h>
#include <stdlib.h>

int	ft_get_index(char c, int base)
{
	char	test[] = "0123456789abcdef";
	int		i = 0;

	if (c >= 65 && c <= 70)
		c = c + 32;
	while (test[i])
	{
		if (test[i] == c && i < base)
			return (i);
		i++;
	}
	return (-1);
}

int	ft_atoi_base(const char *str, int str_base)
{
	int sign;
	int result;
	int num;

	result = 0;
	sign = 1;
	while ((*str >= 9 && *str <= 13) || *str == 32)
		str++;
	if (*str == '-')
		sign = -1;
	if (*str == '+' || *str == '-')
		str++;
	while ((num = ft_get_index(*str, str_base)) != -1)
	{
		result = result * str_base + num;
		str++;
	}
	return (result * sign);
}

int main(int ac, char **av)
{
	printf("%d\n", ft_atoi_base(av[1], atoi(av[2])));
	return (0);
}
