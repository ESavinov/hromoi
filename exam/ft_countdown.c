#include <unistd.h>

int ft_countdown()
{
	int i;

	i = 57;
	while (i >= 48)
	{
		write(1, &i, 1);
		--i;
	}
	return (0);
}

int main()
{
	ft_countdown();
	write(1, "\n", 1);
	return (0);
}
