#include <stdio.h>
#include <stdlib.h>

int ft_is_power_of_2(unsigned int n)
{
	while (n)
	{
		if (n == 1)
			return (1);
		while (n % 2 != 0)
			return (0);
		n = n / 2;
	}
	return (1);
}

int main(int ac, char **av)
{
	int i;
 
	i = ft_is_power_of_2(atoi(av[1]));
	printf("%d\n", i);
	return (0);
}
