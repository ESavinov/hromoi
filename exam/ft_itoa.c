#include <stdlib.h>
#include <stdio.h>

char	*ft_itoa(int nbr)
{
	int		test = nbr;
	int		len = 0;
	int		sign = 0;
	char	*result;
	unsigned int res;

	if (test < 0)
	{
		sign = 1;
		res = nbr * -1;
	}
	else if (test > 0)
		res = nbr;
	else if (test == 0)
	{
		res = nbr;
		len = 1;
	}
	while (test)
	{
		len++;
		test /= 10;
	}
	result = (char*)malloc((len + 1 + sign) * sizeof(char));
	if (sign == 1)
		result[0] = '-';
	result[len + sign] = '\0';
	len--;
	while (len != -1)
	{
		result[len + sign] = (res % 10) + '0';
		res /= 10;
		len--;
	}
	return (result);
}
