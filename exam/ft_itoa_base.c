#include <stdio.h>
#include <stdlib.h>

char	*ft_itoa_base(int value, int base)
{
	int sign = 0;
	int len = 0;
	int temp;
	char *check = "0123456789ABCDEF";

	temp = value;
	char *str;

	if (value < 0)
	{
		if (base == 10)
			sign = 1;
		value *= -1;
	}
	while (temp)
	{
		len++;
		temp /= 10;
	}
	str = (char *)malloc((len + 1 + sign) * sizeof(char));
	str[len + sign] = '\0';
	if (sign == 1)
		str[0] = '-';
	while (len--)
	{
		str[len + sign] = check[value % base];
		value /= base;
	}
	printf("%s\n", str);
	return (str);
}

int		main(void)
{
	ft_itoa_base(0x1010, 10);
	return (0);
}
