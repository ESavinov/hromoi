#include <stdlib.h>
#include <stdio.h>

char *ft_itoa_base(int value, int base)
{
	unsigned int	res;
	int				sign = 0;
	int				len = 0;
	char			*check = "0123456789ABCDEF";
	char			*result;

	if (value < 0)
	{
		res = value * -1;
		if (base == 10)
			sign  = 1;
	}
	else if (value == 0)
	{
		len = 1;
		res = value;
	}
	else
		res = value;
	while (value)
	{
		value /=  base;
		len++;
	}
	result = (char*)malloc(sizeof(char) * len + sign + 1);
	result[sign + len--] = '\0';
	if (sign == 1 && base == 10)
		result[0] = '-';
	while (res)
	{
		result[len-- + sign] = check[res % base];
		res /= base;
	}
	return (result);
}

int main(int ac, char **av)
{
	printf("%s\n", ft_itoa_base(atoi(av[1]), atoi(av[2])));
	return (0);
}
