#ifndef FT_LIST_H
#define FT_LIST_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct	s_list
{
	struct s_list	*next;
	void			*data;
}				t_list;

void	ft_list_foreach(t_list *begin_list, void (*f)(void *));
void	ft_set(void *b);
void	ft_memset(void *b, int c, size_t n);
void	ft_print(t_list *begin_list);

#endif
