#include "ft_list.h" 

void	ft_memset(void *b, int c, size_t len)
{
	char *res;
	int i = 0;

	res = (char *)b;
	while (len--)
		res[i++] = (char)c;
}
