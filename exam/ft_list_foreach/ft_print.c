#include "ft_list.h"

void ft_print(t_list *begin)
{
	while(begin)
	{
		printf("%s\n", begin->data);
		begin = begin->next;
	}
}
