#include "ft_list.h"

int		main()
{
	t_list	*temp = NULL;
	t_list	*begin_list;
	int		len = 5;
	int		flag = 0;

	while (len--)
	{
		if (!flag)
		{
			temp = (t_list *)malloc(sizeof(t_list));
			temp->data = (char *)malloc(sizeof(char) * 5);
			ft_memset(temp->data,'\0', 5);
			ft_memset(temp->data, '1', 4);
			temp->next = (t_list *)malloc(sizeof(t_list));
			temp->next->data = (char *)malloc(sizeof(char) * 5);
			ft_memset(temp->next->data,'\0', 5);
			ft_memset(temp->next->data, '1', 4);
			flag = 1;
			begin_list = temp;
		}
		else
		{
			temp->next = (t_list *)malloc(sizeof(t_list));
			temp->next->data = (char *)malloc(sizeof(char) * 5);
			ft_memset(temp->next->data,'\0', 5);
			ft_memset(temp->next->data, '1', 4);
		}
		temp = temp->next;
	}
	temp->next = NULL;
	ft_print(begin_list);
	ft_list_foreach(begin_list, ft_set);
	ft_print(begin_list);
	return (0);
}
