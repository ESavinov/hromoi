#include <stdlib.h>
#include <stdio.h>

int		ft_count_word(char *str)
{
	int i;

	i = 0;
	while (*str)
	{
		while ((*str == ' ' || *str == '\n' || *str == '	') && *str)
			str++;
		if (*str != ' ' && *str != '\n' && *str != '	' && *str)
			i++;	
		while (*str != ' ' && *str != '\n' && *str != '	' && *str)
			str++;
	}
	return (i);
}

char	*ft_malloc_str(char *str, int start)
{
	int		i;
	char 	*test;
	char	temp[1000];

	i = 0;
	while (str[start] != ' ' && str[start] != '\n' && str[start] != '	' && str[start])
	{
		temp[i] = str[start];
		i++;
		start++;
	}
	temp[i] = '\0';
	i = 0;
	while (temp[i])
		i++;
	if (!(test = (char *)malloc((i + 1) * sizeof(char))))
		return (NULL);
	i = 0;
	while(temp[i])
	{
		test[i] = temp[i];
		i++;
	}
	test[i] = '\0';
	return (test);
}

char	**ft_split(char *str)
{
	char	**temp;
	int		i;
	int		j;
	int		k;

	j = 0;
	k = 0;
	i = ft_count_word(str);
	if (!(temp = (char **)malloc((i + 1) * sizeof(char *))))
		return (NULL);
	while(str[k])
	{
		while(str[k] && (str[k] == ' ' || str[k] == '	' || str[k] == '\n'))
			k++;
		if (str[k])
		{
			temp[j] = ft_malloc_str(str, k);
			j++;
		}
		while(str[k] != ' ' && str[k] != '\n' && str[k] != '	' && str[k])
			k++;
	}
	temp[j] = NULL;
	return (temp);
}

int main(int ac, char **av)
{
	char **test;

	test = ft_split(av[1]);
	while (*test)
	{
		printf("%s\n", *test);
		test++;
	}
	return (0);
}
