#include <stdio.h>
#include <unistd.h>

char	ft_down(char av)
{
	return (av + 32);
}

char	ft_up(char av)
{
	return (av - 32);
}

void	ft_str_capitalizer(char *av)
{
	int	i;
	int	flag = 0;
	char	buff[1000];

	i = 0;
	while (*av)
	{
		while (*av == 9 || *av == 32)
		{
			buff[i] = *av;
			av++;
			i++;
		}
		while (*av < 65 || (*av > 90 && *av < 97) || *av > 122)
		{
			buff[i] = *av;
			av++;
			i++;
			flag = 1;
		}
		if (*av >= 97 && *av <= 122 && flag == 0)
		{
			buff[i] = ft_up(*av);
			av++;
			i++;
		}
		else if (*av >= 65 && *av <= 90 && flag == 0)
		{
			buff[i] = *av;
			av++;
			i++;
		}
		while ((*av >= 97 && *av <= 122) || (*av >= 65 && *av <= 90))
		{
			if (*av >= 65 && *av <= 90)
				buff[i] = ft_down(*av);
			else
				buff[i] = *av;
			av++;
			i++;
			flag = 0;
		}
	}
	buff[i] = '\0';
	printf("%s\n", buff);
}

int main(int ac, char **av)
{
	int i;

	i = 1;
	if (ac != 1)
	{
		while (i != ac)
		{
			ft_str_capitalizer(av[i]);
			i++;
		}
	}
	else
		write (1, "\n", 1);
	return (0);
}
