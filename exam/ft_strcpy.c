
#include <stdio.h>

char *ft_strcpy(char *src, char *dest)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int main()
{
	char src [] = "Hello world";
	char dest [] = "Hello wow world";
	ft_strcpy(src, dest);
	printf("%s", ft_strcpy(src, dest));
	return (0);
}
