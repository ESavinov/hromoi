#include <stdio.h>
#include <string.h>

size_t	ft_strcspn(char *av1, char *av2)
{
	size_t	result = 0;
	char	*temp;

	temp = av2;
	while (*av1)
	{
		if (*av1 != *av2)
		{
			av2++;
			if(!*av2)
			{
				result++;
				av1++;
				av2 = temp;
			}
		}
		else
			return (result);
	}
	return (result);
}

int		main(int ac, char **av)
{
	printf("original - %lu\n", strcspn(av[1], av[2]));
	printf("custom - %lu\n", ft_strcspn(av[1], av[2]));
	return (0);
}
