#include <stdio.h>

char	*ft_strrev(char *av)
{
	int		index = 0;
	int		len = 0;
	char	temp[100];
	char	*res;

	while(av[index++])
		len++;
	len -= 1;
	index = 0;
	res = temp;
	while(len != -1)
		temp[index++] = av[len--];
	temp[index] = '\0';
	return(res);
}

int		main(int ac, char **av)
{
	printf("%s\n", ft_strrev(av[1]));
	return (0);
}
