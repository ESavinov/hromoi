#include <stdio.h>
#include <string.h>

size_t ft_strspn(char *av1, char *av2)
{
	char *temp;
	size_t result = 0;

	temp = av2;
	while (*av1 && *av2)
	{
		if (*av1 != *av2 && *av2)
			av2++;
		else if (*av1 == *av2 && *av2)
		{
			av1++;
			result++;
			av2 = temp;
		}
	}
	return (result);
}

int main(int ac, char **av)
{
	printf("original - %lu\n", strspn(av[1], av[2]));
	printf("custom - %lu\n", ft_strspn(av[1], av[2]));
	return (0);
}
