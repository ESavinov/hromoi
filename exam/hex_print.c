#include <unistd.h>
#include <stdio.h>

int	ft_atoi(char *av)
{
	int	result;

	result = 0;
	if (*av == '+')
		av++;
	while (*av >= 48 && *av <= 57)
	{
		if (result != (result * 10) / 10)
			return (0);
		result = result * 10;
		if (result != result + (*av - '0') - (*av - '0'))
			return (0);
		result = result + (*av - '0');
		av++; 
	}
	return (result);
}

void	print_hex(int hex)
{
	char	h;

	if (hex > 16)
	{
		print_hex(hex / 16);
		print_hex(hex % 16);
	}
	if (hex < 10)
	{
		h = hex + '0';
		write(1, &h, 1);
	}
	if (hex > 9 && hex < 16)
	{
		h = hex + 'W';
		write(1, &h, 1);
	}
}

int	main(int ac, char **av)
{
	int	hex;

	if (ac == 2)
	{
		if (!(hex = ft_atoi(av[1])))
			write(1, "\n", 1);
		print_hex(hex);
		write(1, "\n", 1);
	}
	else
		write(1, "\n", 1);
	return (0);
}
