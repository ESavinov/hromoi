#include <unistd.h>

int	ft_hidenp(char *av1, char * av2)
{
	while (*av1)
	{
		while (*av1 != *av2)
		{
			av2++;
			if (!*av2)
			{
				write(1, "0\n", 2);
				return (0);
			}
		}
		if (*av1 == *av2)
		{
			av1++;
			av2++;
			if (*av1 && !*av2)
			{
				write(1, "0\n", 2);
				return (0);
			}
		}
	}
	write(1, "1\n", 2);
	return (0);
}

int	main(int ac, char **av)
{
	if (ac == 3)
		ft_hidenp(av[1], av[2]);
	else
		write(1, "\n", 1);
	return (0);
}
