#include <unistd.h>
#include <stdio.h>
int main(int argc, char **argv)
{
	if (argc != 3)
		write(1, "\n", 1);
	else
	{
		int i;
		int j;
		int a;
		int f;
		int c;

		i = 0;
		while(argv[1][i] != '\0')
		{
			c = 0;
			j = 0;
			while(argv[2][j] != '\0' && c == 0)
			{
				if(argv[1][i] == argv[2][j])
				{
					a = 0;
					f = 0;
					while (i > a && f == 0)
					{
						if(argv[1][i] == argv[1][a])
						{
							a++;
							f = 1;
						}
						else
							a++;
					}
					if (f == 0)
					{
						write(1, &argv[1][i], 1);
						c = 1;
					}
					else
						c = 1;
				}
				else
					j++;
			}
			i++;
		}
		write(1, "\n", 1);
	}
	return (0);
}
