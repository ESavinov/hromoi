#include <unistd.h>

int main(int argc, char **argv)
{
	int j;

	j = 0;
	if(argc != 2)
		write(1, "\n", 1);
	else
	{
		while (argv[1][j] != '\0')
			j++;
		j--;
		while (j > 0 && (argv[1][j] == 32 || argv[1][j] == 11))
			j--;
		while (argv[1][j] != 32 && argv[1][j] != 11)
			j--;
		j++;
		while (argv[1][j] != 32 && argv[1][j] != 11 && argv[1][j] != '\0')
		{
			write(1, &argv[1][j], 1);
			j++;
		}
		write(1, "\n", 1);
	}
	return (0);
}
