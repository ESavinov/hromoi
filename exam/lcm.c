#include <unistd.h>

unsigned int	ft_lcm(unsigned int i, unsigned int j)
{
	int		del;

	i >= j ? (del = i) : (del = j);
	if (i == 0 || j == 0)
		write(1, "0\n", 2);
	while (del % i != 0 || del % j != 0)
		del++;
	return (del);
}
