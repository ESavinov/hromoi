#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char	*ft_itoa(unsigned int d)
{
	int temp = d;
	int len = 0;
	char *res;

	if (!temp)
		len = 1;
	while (temp)
	{
		temp /= 10;
		len++;
	}
	res = malloc((len + 1) * sizeof(*res));
	res[len--] = '\0';
	while (len != -1)
	{
		res[len] = (d % 10) + '0';
		d /= 10;
		len--;
	}
	return (res);
}

char	*ft_minutes(int d)
{
	char *res;

	d = d / 60;
	res = ft_itoa(d);
	return (res);
}

char	*ft_hours(int d)
{
	char *res;

	d = d / 60;
	d = d / 60;
	res = ft_itoa(d);
	return (res);
}

char	*ft_days(int d)
{
	char *res;

	d = d / 60;
	d = d / 60;
	d = d / 24;
	res = ft_itoa(d);
	return (res);
}

char	*ft_month(int d)
{
	char *res;

	d = d / 60;
	d = d / 60;
	d = d / 24;
	d = d / 30;
	res = ft_itoa(d);
	return (res);
}

char *resi(char *res, char *sec)
{
	char	*result;
	char	*tmp;
	int		i = 0;

	tmp = res;
	result = malloc((strlen(sec) + strlen(res) + 1) * sizeof(*result));
	while (*res)
		result[i++] = *res++;
	while (*sec)
		result[i++] = *sec++;
	result[i] = '\0';
	free (tmp);
	return (result);
}

char *ft_result(char *res, int temp)
{
	char *result;

	
	if (temp == 1)
	{
		if (*res == '1' && *(res + 1) == '\0')
			result = resi(res, " second ago.");
		else
			result = resi(res, " seconds ago.");
	}
	if (temp == 2)
	{
		if (*res == '1' && *(res + 1) == '\0')
			result = resi(res, " minute ago.");
		else
			result = resi(res, " minutes ago.");
	}
	if (temp == 3)
	{
		if (*res == '1' && *(res + 1) == '\0')
			result = resi(res, " hour ago.");
		else
			result = resi(res, " hours ago.");
	}
	if (temp == 4)
	{
		if (*res == '1' && *(res + 1) == '\0')
			result = resi(res, " day ago.");
		else
			result = resi(res, " days ago.");
	}
	if (temp == 5)
		result = resi(res, " month ago.");
	return (result);
}

char	*moment(unsigned int d)
{
	char	*res;

	if (d < 60)
	{
		res = ft_itoa(d);
		res = ft_result(res, 1);
	}
	else if (d < 3600)
	{
		res = ft_minutes(d);
		res = ft_result(res, 2);
	}
	else if (d < 216000)
	{
		res = ft_hours(d);
		res = ft_result(res, 3);
	}
	else if (d < 5184000)
	{
		res = ft_days(d);
		res = ft_result(res, 4);
	}
	else
	{
		res = ft_month(d);
		res = ft_result(res, 5);
	}
	return (res);
}

int		main(int ac, char **av)
{
	printf("%s\n", moment(atoi(av[1])));
	ac = 0;
	return (0);
}
