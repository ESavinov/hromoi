#include <unistd.h>

void options(char **av, int ac)
{
	int size = 31;
	int i = 0;
	int count = ac - 1;
	int j = 1;
	char arr[] = "00000000000000000000000000000000";

	while (count >= j)
	{
		if (av[j][i++] == '-')
		{
			while (av[j][i])
			{
				if (av[j][i] == 'h')
				{
					write(1, "options: abcdefghijklmnopqrstuvwxyz\n", 36);
					return ;
				}
				if (av[j][i] < 'a' || av[j][i] > 'z')
				{
					write(1, "Invalid Option\n", 15);
					return ;
				}
				else
					arr[av[j][i] - 97] = 49;
				i++;
			}
		}
		else
		{
			write(1, "Invalid Option\n", 15);
			return ;
		}
		j++;
		i = 0;
	}
	while (size != -1)
	{
		write (1, &arr[size], 1);
		if (size % 8 == 0)
			write (1, " ", 1);
		size--;
	}
	write (1, "\n", 1);
}

int main(int ac, char **av)
{
	if (ac > 1)
		options(av, ac);
	else
		write(1, "options: abcdefghijklmnopqrstuvwxyz\n", 36);
	return (0);
}
