#include <unistd.h>
#include <stdlib.h>

void ft_paramsum(int count)
{
	char c;
	if (count > 9)
	{
		ft_paramsum(count / 10);
		ft_paramsum(count % 10);
	}
	else
	{
		c = count + '0';
		write(1, &c, 1);
	}
}

int main(int ac, char **av)
{
	if (ac > 1)
	{
		ft_paramsum(ac - 1);
		write(1, "\n", 1);
	}
	else
		write(1, "0\n", 2);
	av = NULL;
	return (0);
}
