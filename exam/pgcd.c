#include <stdlib.h>
#include <stdio.h>

void	ft_pgcd(int i, int j)
{
	int k = 1;
	int temp;
	int result = 0;

	i <= j ? (temp = i) : (temp = j);
	while(k <= temp)
	{
		if (i % k == 0 && j % k == 0)
		{
			result = k;
			k++;
		}
		else
			k++;
	}
	printf("%d\n", result);
}

int main(int ac, char **av)
{
	if (ac == 3)
		ft_pgcd(atoi(av[1]), atoi(av[2]));
	else
		printf("%c", '\n');
	return (0);
}
