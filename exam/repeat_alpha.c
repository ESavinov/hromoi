#include <unistd.h>

int main(int argc, char **argv)
{
	int i;
	int j;
	int index1;
	int index2;

	j = 0;
	i = 0;
	if (argc > 2 || argc == 1)
	{
		write(1, "\n", 1);
	}
	else
	{
		while(argv[1][i] != '\0')
		{
			index1 = argv[1][i] - 65 + 1;
			index2 = argv[1][i] - 97 + 1; 
			if (argv[1][i] >= 65 && argv[1][i] <= 90)
				while (index1 > j)
				{
					write (1, &argv[1][i], 1);
					index1--;
				}
			else if (argv[1][i] >= 97 && argv[1][i] <= 123)
				while (index2 > j)
				{
					write (1, &argv[1][i], 1);
					index2--;
				}
			else
				write (1, &argv[1][i], 1);
			i++;
		}
		write(1, "\n", 1);
	}
	return (0);
}
