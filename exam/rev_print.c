#include <unistd.h>

int main(int argc, char **argv)
{
	int i;
	int j;

	if (argc != 2)
		write(1, "\n", 1);
	else
	{
		i = 0;
		j = 0;
		while (argv[1][i] != '\0')
			i++;
		while (i > j)
		{
			write(1, &argv[1][i - 1], 1);
			i--;
		}
		write(1, "\n", 1);
	}
	return (0);
}
