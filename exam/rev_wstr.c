#include <unistd.h>
#include <stdlib.h>

void    ft_rev_str(char *av)
{
    int i = 0;
    int j = 0;
    int k;
    int len;
    int print_len;
    char temp[1000];

    while (av[i])
        i++;
    len = i;
    print_len = len;
    i--;
    while (len)
    {
        while (av[i] != 32 && av[i] != 9 && len)
        {
            i--;
            len--;
            if (len == 0)
                break;
        }
        k = i + 1;
        while (av[k] != '\0' && av[k] != 32 && av[k] != 9)
        {
            temp[j] = av[k];
            j++;
            k++;
        }
        i--;
        if (len)
            len--;
        if (len)
        {
            temp[j] = 32;
            j++;
        }
    }
    temp[j] = '\0';
    write(1, &temp, print_len);
    write(1, "\n", 1);
}

int     main(int ac, char **av)
{
    if (ac == 2)
        ft_rev_str(av[1]);
    else
        write(1, "\n", 1);
    return (0);   
}
