
#include <unistd.h>

int main(int argc, char **argv)
{
	int i;
	int j;
	int u;

	i = 0;
	j = 0;
	u = 0;
	while (argv[2][i] != '\0')
		i++;
	while (argv[3][j] != '\0')
		j++;
	if ((i > 1 && j > 1) || argc != 4)
		write(1, "\n", 1);
	else
	{
		i = 0;
		j = 0;
		while (argv[1][u] != '\0')
		{
			if (argv[1][u] != argv[2][i])
			{
				write(1, &argv[1][u], 1);
				u++;
			}
			else
			{
				argv[1][u] = argv[3][j];
				write(1, &argv[1][u], 1);
				u++;
			}
		}
	}
	return (0);
}
