#include "ft_sort.h"

void	ft_print(int *tab, unsigned int size)
{
	while (size--)
		printf("%d\n", *tab++);
}
