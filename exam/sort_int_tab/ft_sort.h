#ifndef FT_SORT_H
#define FT_SORT_H

#include <stdio.h>

void    ft_print(int *tab, unsigned int size);
void    sort_int_tab(int *tab, unsigned int size);
void	swap(int *tab);

#endif
