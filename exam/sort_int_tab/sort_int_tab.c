#include "ft_sort.h"

void sort_int_tab(int *tab, unsigned int size)
{
	unsigned int	temp = size - 1;
	int				*begin;

	begin = tab;
	while (size--)
	{
		while (temp--)
		{
			if (*tab > *(tab + 1))
				swap(tab);
			tab++;
		}
		tab = begin;
		temp = size - 1; 
	}
}
