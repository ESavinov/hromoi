#include "ft_sort.h"

void	swap(int *tab)
{
	int tmp;

	tmp = *tab;
	*tab = *(tab + 1);
	*(tab + 1) = tmp;
}
