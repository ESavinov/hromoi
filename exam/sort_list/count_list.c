#include "list.h"

int count_list(t_list *lst)
{
	int i = 0;

	while (lst)
	{
		i++;
		lst = lst->next;
	}
	return (i);
}
