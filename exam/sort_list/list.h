#ifndef LIST_H
#define LIST_H

#include <stdio.h>

typedef struct		s_list
{
	int				i;
	struct s_list	*next;
}					t_list;

void				ft_print(t_list *lst);
int					count_list(t_list *lst);
t_list				*sort_list(t_list *lst, int (*cmp)(int, int));

#endif
