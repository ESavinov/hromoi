#include "list.h"

int ascending(int a, int b)
{
	return (a <= b);
}

int main(void)
{
	t_list lst5 = {5, NULL};
	t_list lst4 = {6, &lst5};
	t_list lst3 = {2, &lst4};
	t_list lst2 = {1, &lst3};
	t_list lst1 = {3, &lst2};
	t_list lst = {4, &lst1};
	t_list *res;

	ft_print(&lst);
	printf("\n");
	res = sort_list(&lst, ascending);
	ft_print(res);
	return (0);
}
