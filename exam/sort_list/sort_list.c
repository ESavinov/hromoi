#include "list.h"

t_list *sort_list(t_list *lst, int (*cmp)(int, int))
{
	t_list	*temp;
	t_list	*begin;
	t_list	*prev;
	int		j;

	j = count_list(lst);
	begin = lst;
	while (j--)
	{
		while (lst->next)
		{
			if (cmp(lst->i, lst->next->i) == 0)
			{
				if (begin == lst)
				{
					temp = lst->next;
					lst->next = temp->next;
					temp->next = lst;
					begin = temp;
					prev = temp;
				}
				else
				{
					temp = lst->next;
					lst->next = temp->next;
					temp->next = lst;
					prev->next = temp;
					prev = prev->next;
				}
			}
			else
			{
				prev = lst;
				lst = lst->next;
			}
		}
		lst = begin;
	}
	return (lst);
}
