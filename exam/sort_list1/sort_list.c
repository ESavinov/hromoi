#include "sort_list.h"

void	ft_print_s_next(t_list *lst)
{
	t_list *temp;

	temp = lst;
	while (temp)
	{
		printf("%d\n", temp->x);
		temp = temp->next;
	}
}

void	ft_print_s_prev(t_list *lst)
{
	while (lst)
	{
		printf("%d\n", lst->x);
		lst = lst->previous;
	}
}

int cmp(int a, int b)
{
	return (a >= b);
}

t_list *ft_create_spisok(t_list *lst, int len)
{
	int		id;
	t_list	*new;
	t_list	*previous;

	id = 1;
	while (len-- != 1)
	{
		if (!lst)
		{
			new = (t_list *)malloc(sizeof(t_list));
			new->x = id++;
			new->previous = NULL;
			lst = new;
		}
		new->next = (t_list *)malloc(sizeof(t_list));
		new->next->x = id++;
		// previous = new;
		new = new->next;
		// new->previous = previous;
	}
	new->next = NULL;
	return (lst);
}

int	ft_slen(t_list *lst)
{
	int		i;
	t_list	*spisok;

	i = 0;
	spisok = lst;
	while (spisok)
	{
		if (!spisok->next)
			break;
		spisok = spisok->next;
	}
	return (spisok->x);
}

t_list	*sort_list(t_list *lst, int (*cmp)(int a, int b))
{
	int		temp;
	int		length;
	t_list	*tmp;
	t_list	*elem;
	t_list	*prev;

	length = ft_slen(lst);
	while (length--)
	{
		elem = lst;
		while (elem->next)
		{
			if ((*cmp)(elem->x, elem->next->x) == 0)
			{
				if (elem == lst)
				{
					tmp = elem;
					elem = tmp->next;
					tmp->next = elem->next;
					elem->next = tmp;
					lst = elem;
				}
				else
				{
					tmp = elem;
					elem = tmp->next;
					tmp->next = elem->next;
					elem->next = tmp;
					prev->next = elem;
				}
			}
			prev = elem;
			elem = elem->next;
		}
	}
	return (lst);
}

int main()
{
	t_list	*lst;
	int		len;

	len = 5;
	lst = NULL;
	lst = ft_create_spisok(lst, len);
	ft_print_s_next(lst);
	lst = sort_list(lst, &cmp);
	ft_print_s_next(lst);
	return (0);
}