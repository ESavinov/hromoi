#ifndef SORT_LIST_H
# define SORT_LIST_H

#include <stdlib.h>
#include <stdio.h>

typedef struct      s_list
{
    int             x;
    struct s_list   *next;
    struct s_list   *previous;
}                   t_list;


#endif