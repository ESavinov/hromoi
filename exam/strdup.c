#include <stdlib.h>
#include <stdio.h>
int ft_strlen(char *str)
{
	int i;

	i = 0;
	while(str[i] != '\0')
	{
		i++;
	
	}
	i++;
	return (i);
}

char* ft_strdup(char *str, int i)
{
	char* str1;
	int j;

	j = 0;
	str1 = (char *)malloc(sizeof(char) * i);
	if (str1 == NULL)
		return NULL;
	else
	{
		while(str[j] != '\0')
		{
			str1[j] = str[j];
			j++;
		}
		str1[j] ='\0';
	}
	return (str1);
}

int main()
{
	char str [] = "Hello world";
	printf("%s", ft_strdup(str, ft_strlen(str)));
	return (0);
}
