#include <unistd.h>

void	ft_itoa(int result)
{
	char	tmp;

	if (result > 9)
	{
		ft_itoa(result / 10);
		ft_itoa(result % 10);
	}
	if (result <= 9)
	{
		tmp = result + '0';
		write(1, &tmp, 1);
	}
}

void	ft_tab_mult(int result)
{
	int	x;
	int	i;
	int	chislo;
	char	tmp;
	char	tmp1;

	x = 1;
	i = 0;
	while (x <= 9)
	{
		chislo = x * result;
		tmp = x + '0';
		write(1, &tmp, 1);
		write(1, " x ", 3);
		ft_itoa(result); 
		write(1, " = ", 3);
		ft_itoa(chislo);
		write(1, "\n", 1);
		x++;
	}
}

int	ft_atoi(char *av)
{
	int result;

	result = 0;
	while (*av >= 48 && *av <= 57)
	{
		result = result * 10 + (*av - '0');
		av++;
	}
	return (result);
}

int	main(int ac, char **av)
{
	if (ac == 2)
		ft_tab_mult(ft_atoi(av[1]));
	else
		write(1, "\n", 1);
	return (0);
}
