package com.company.beans;

public class Address {
    private String name;
    private String numberOfHouse;

    @Override
    public String toString() {
        return "Address{" +
                "name='" + name + '\'' +
                ", numberOfHouse='" + numberOfHouse + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberOfHouse() {
        return numberOfHouse;
    }

    public void setNumberOfHouse(String numberOfHouse) {
        this.numberOfHouse = numberOfHouse;
    }
}
