package com.company.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:cat.properties")
public class Cat {
    @Value(value="${cat.name}")
    private String name;
    @Value(value="${cat.age}")
    private int age;

    @Override
    public String toString() {
        return this.name + " " + this.age;
    }
}
