package com.company.beans;

public class House2 {
    private Address address;

    @Override
    public String toString() {
        return "House2{" +
                "address2=" + address +
                '}';
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress3(Address address) {
        this.address = address;
    }
}
