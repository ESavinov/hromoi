package com.company.beans;

import com.company.beans.Car;
import com.company.beans.Cat;
import com.company.beans.House;
import com.company.beans.House2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config.xml");
//        Car car = applicationContext.getBean("firstBeanCar", Car.class);
//        System.out.println(car.getMark() + " " + car.getSpeed());
        Car car2 = applicationContext.getBean("secondBeanCar", Car.class);
        System.out.println(car2.getMark() + " " + car2.getSpeed());
//        System.out.println(car == car2);

        ApplicationContext applicationContext1 = new AnnotationConfigApplicationContext("com.company.beans");
        Cat cat = applicationContext1.getBean(Cat.class);
        System.out.println(cat.toString());
        House house = (House)applicationContext.getBean("house");
        System.out.println(house.toString());
        House2 house2 = (House2)applicationContext.getBean("house2");
        System.out.println(house2.toString());
        System.out.println(house2.getAddress().toString());
    }
}
