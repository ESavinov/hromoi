package com.company.lesson2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class House {
    private int countOfFlats;
    private String city;
    private Address address;

    @Override
    public String toString() {
        return "House{" +
                "countOfFlats=" + countOfFlats +
                ", city='" + city + '\'' +
                ", address=" + address +
                '}';
    }

    public Address getAddress() {
        return address;
    }

    public int getCountOfFlats() {
        return countOfFlats;
    }

    public String getCity() {
        return city;
    }

    @Autowired
    public House(Address address, @Value(value="Moscow") String city, @Value(value="5") int countOfFlats){
        this.city = city;
        this.address = address;
        this.countOfFlats = countOfFlats;
    }

//    @Autowired
//    public void setCity(Address address, @Value(value="Moscow") String city, @Value(value="5") int countOfFlats) {
//        this.city = city;
//        this.address = address;
//        this.countOfFlats = countOfFlats;
//    }
}
