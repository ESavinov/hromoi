package com.company.lesson2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] Args) {
        ApplicationContext appCont = new AnnotationConfigApplicationContext(com.company.lesson2.House.class, com.company.lesson2.Address.class);
        House house = appCont.getBean("house", House.class);
        System.out.println(house.toString());
        Address address = appCont.getBean("address", Address.class);
        System.out.println(address.toString());
    }
}
