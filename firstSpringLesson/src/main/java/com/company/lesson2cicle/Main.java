package com.company.lesson2cicle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] Args){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Parent.class, Child.class, Family.class);
        Family family = applicationContext.getBean("family", Family.class);
//        Child child = applicationContext.getBean("child", Child.class);
        Parent parent = applicationContext.getBean("parent", Parent.class);
        System.out.println(parent);
        System.out.println(family);
    }
}
