package com.company.lesson3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Admin {
    @Value(value="1234")
    private int password;
    @Value(value="root")
    private String name;

    @Override
    public String toString() {
        return "Admin{" +
                "password=" + password +
                ", name='" + name + '\'' +
                '}';
    }
}
