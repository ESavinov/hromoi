package com.company.lesson3;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class AdminService {
    private Admin admin;
    private DataBase dataBase;

    @Autowired
    public AdminService(Admin admin, DataBase dataBase) {
        this.dataBase = dataBase;
        this.admin = admin;
        dataBase.setAdmin(admin);
    }

//    public AdminService(){
//    }

    @PostConstruct
    public void initBase(){
        dataBase.setAdmin(admin);
    }

    @PreDestroy
    public void close(){
        dataBase.closeConnection();
    }

//    @Autowired
//    public void setAdmin(Admin admin){
//        this.admin = admin;
//    }

    public Admin getAdmin(){
        return admin;
    }
//    @Autowired
//    public void setDataBase(DataBase dataBase){
//        this.dataBase = dataBase;
//    }

    public DataBase getDataBase(){
        return dataBase;
    }

}
