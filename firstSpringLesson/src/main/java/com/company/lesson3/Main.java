package com.company.lesson3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Admin.class, AdminService.class, DataBase.class);
//        AdminService adminService = applicationContext.getBean("adminService", AdminService.class);
        AnnotationConfigApplicationContext annConApp = applicationContext;
        annConApp.close();
    }
}
