package com.company.lesson6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.File;

@Configuration
public class Config {
    @Bean
    public Logger fileLogger(){
        return new FileLogger(file());
    }

    @Bean
    public Logger consoleLogger(){
        return new ConsoleLogger();
    }

    @Bean
    public File file(){
        return new File("hey");
    }
}
