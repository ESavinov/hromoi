package com.company.lesson6;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

public class FileLogger implements Logger {
    private File file;

    public FileLogger(File file) {
        this.file = file;
    }

    public FileLogger(){

    }

    @Override
    public void write(){
        System.out.println("we write to file " + file + " hello");
    }
}
