package com.company.lesson6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FirstFileLogger implements Logger {

    private ConsoleLogger consoleLogger;

//    @Autowired
//    public void setFirstFileLogger(ConsoleLogger consoleLogger) {
//        this.consoleLogger = consoleLogger;
//    }
//
//    public ConsoleLogger getConsoleLogger(){
//        return this.consoleLogger;
//    }


    @Autowired
    public FirstFileLogger(ConsoleLogger consoleLogger){
        this.consoleLogger = consoleLogger;
    }

    @Override
    public void write(){
        System.out.println("we write FirstFileLogger " + consoleLogger + " component");
    }
}
