package com.company.lesson6;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class, FirstFileLogger.class);
        FirstFileLogger firstFileLogger = applicationContext.getBean("firstFileLogger", FirstFileLogger.class);
        firstFileLogger.write();
    }
}
