package com.company.lesson_5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Car {
    private Door door;
    private Door door_2;

    @Autowired
    public Car(Door door, Door door_2){
        this.door = door;
        this.door_2 = door_2;
    }
//    @Autowired
//    public void setDoor(Door door){
//        this.door = door;
//    }
//
//    public Door getDoor(){
//        return this.door;
//    }
//
//    @Autowired
//    public void setDoor_2(Door door_2){
//        this.door_2 = door_2;
//    }
//
//    public Door getDoor_2(){
//        return this.door_2;
//    }

    @Override
    public String toString() {
        return (door == door_2 ? "true" : "false");
    }
}
