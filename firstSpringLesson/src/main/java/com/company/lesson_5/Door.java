package com.company.lesson_5;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class Door {
    public void printDoor(){
        System.out.println("door");
    }
}
