package com.company.lesson_5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Service {
    @Autowired
    private Car car;

    @Override
    public String toString(){
        return "Hello" + car;
    }
}
