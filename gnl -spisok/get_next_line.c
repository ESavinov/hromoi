#include "get_next_line.h"
#include "stdlib.h"
#include <stdio.h>
#include <libc.h>

int	get_next_line(int fd, char **line)
{
	char			*buff;
	char			*konk;
	char			*endstr;
	static t_list	*root;
	t_list			*temp;
	t_list			*test;
	t_list			*previous;
	ssize_t			bytes;

	if (!line || !(buff = (char*)malloc(BUFF_SIZE + 1)))
	{
		ft_strdel(&buff);
		return (-1);
	}
	temp = root;
	while (temp != NULL)
	{
		if (temp->fd == fd)
			break ;
		temp = temp->next;
	}
	if (temp == NULL)
	{
		if (!(temp = (t_list *)malloc(sizeof(t_list))))
			return (-1);
		if (!(temp->ostatok = (char *)malloc(1)))
			return (-1);
		temp->fd = fd;
		ft_bzero(temp->ostatok, 1);
		temp-> next = root;
		root = temp;
	}
	konk = temp->ostatok;
	if ((endstr = ft_strchr(temp->ostatok, '\n')) == NULL)
	{
		while ((bytes = read(fd, buff, BUFF_SIZE)) > 0)
		{
			buff[bytes] = '\0';
			if (!(konk = ft_strjoin(temp->ostatok, buff)))
			{
				ft_strdel(&(temp->ostatok));
				return (-1);
			}
			ft_strdel(&(temp->ostatok));
			if ((endstr = ft_strchr(konk, '\n')) != NULL)
				break ;
			temp->ostatok = konk;
		}
		if (bytes == -1)
			return (-1);
	}
	ft_strdel(&buff);
	if (konk && *konk)
	{
		if (endstr != NULL)
		{
			if (!(*line = ft_strsub(konk, 0, ft_strlen(konk) - ft_strlen(endstr))) || !(temp->ostatok = ft_strsub(endstr, 1, ft_strlen(endstr))))
			{
				ft_strdel(&konk);
				return(-1);
			}
			ft_strdel(&konk);
			return (1);
		}
		else
		{
			*line = ft_strdup(temp->ostatok);
			ft_putendl(*line);
			ft_strdel(&temp->ostatok);
			temp->ostatok = ft_strnew(0);
			return (1);
		}
	}
	temp = root;
	previous = NULL;
	while (temp != NULL)
	{
		if (temp->fd == fd && previous == NULL)
		{
			root = temp->next;
			ft_strdel(&temp->ostatok);
			free(temp);
			break ;
		}
		else if (temp->fd == fd && previous != NULL)
		{
			previous->next = temp->next;
			ft_strdel(&temp->ostatok);
			free(temp);
			break ;
		}
		else
		{
			previous = temp;
			temp = temp->next;
		}
	}
	return (0);
}

// int	main()
// {
// 	int fd = open("1.txt", O_RDONLY);
// 	char *line;
// 	printf("result - %d\n", get_next_line(fd, &line));
// 	ft_strdel(&line);
// 	printf("result - %d\n", get_next_line(fd, &line));
// 	ft_strdel(&line);
// 	printf("result - %d\n", get_next_line(fd, &line));
// 	ft_strdel(&line);
// 	printf("result - %d\n", get_next_line(fd, &line));
// 	ft_strdel(&line);
// 	return (0);
// }
