/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 14:51:27 by yperra-f          #+#    #+#             */
/*   Updated: 2019/01/25 14:36:49 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(char*, ssize_t))
{
	if (!alst || !del)
		return ;
	if (*alst)
	{
		del((*alst)->ostatok, (*alst)->fd);
		free(*alst);
		*alst = NULL;
	}
}
