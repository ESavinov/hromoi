/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 17:49:09 by yperra-f          #+#    #+#             */
/*   Updated: 2019/01/25 13:18:34 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(char *ostatok, ssize_t fd)
{
	t_list	*new;

	if ((new = (t_list *)malloc(sizeof(t_list))) == NULL)
		return (NULL);
	if (!(new->ostatok = malloc(ft_strlen(ostatok))))
	{
		free(new);
		return (NULL);
	}
	new->ostatok = ft_strcpy(new->ostatok, ostatok);
	new->fd = fd;
	new->next = NULL;
	return (new);
}
