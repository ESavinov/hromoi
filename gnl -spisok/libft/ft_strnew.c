/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 13:04:41 by yperra-f          #+#    #+#             */
/*   Updated: 2019/01/25 13:02:57 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char *s;

	if (size + 1 == 0)
		return (NULL);
	s = NULL;
	s = (char *)malloc((size + 1) * sizeof(char));
	if (s == 0)
		return (NULL);
	ft_memset(s, '\0', size + 1);
	return (s);
}
