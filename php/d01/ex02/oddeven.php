#!/usr/bin/php
<?php

do
{
    printf("Enter a number : ");
    $buff = (fgets(STDIN));
    $res = trim($buff);
    if (is_numeric($res))
    {
        if ($res[strlen($res) - 1] % 2 == 0)
            printf("The number $res is even\n");
        else
            printf("The number $res is odd\n");
    }
    else if ($buff !== false)
        echo("$res is not a number\n");
} while ($buff !== false);
    echo "\n";

?>