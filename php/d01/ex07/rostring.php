#!/usr/bin/php
<?php

if ($argc > 1)
{
    $res = trim(preg_replace("/ +/", " ", $argv[1]));
    $res = explode(" ", $res);
    $temp = $res[0];
    $res = array_slice($res, 1);
    $res[] = $temp;
    $res = implode(" ", $res);
    echo $res."\n";
}

?>