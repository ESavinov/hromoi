#!/usr/bin/php
<?php

function cmp($a, $b){
    $res = "abcdefghijklmnopqrstuvwxyz0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    for ($i = 0; $i < strlen($a) || $i < strlen($b); $i++){
        if (stripos($res, $a[$i]) == stripos($res, $b[$i]))
            continue;
        return (stripos($res, $a[$i]) < stripos($res, $b[$i]) ? -1 : 1);
    }
    if (strlen($a) < strlen($b))
        return(0);
    else
        return (1);
}

$massiv = [];

for ($i = 1; $i <= $argc - 1; $i++){
    $argv[$i] = trim(preg_replace("/ +/", " ", $argv[$i]));
    $argv[$i] = explode(" ", $argv[$i]);
    foreach($argv[$i] as $elem){
        $massiv[] = $elem;
    }
}

usort($massiv, "cmp");

foreach($massiv as $elem){
    echo $elem."\n";
}
?>