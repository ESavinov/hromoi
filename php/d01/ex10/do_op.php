#!/usr/bin/php
<?php

if ($argc == 4)
{
    $res1 = trim($argv[1]);
    $res2 = trim($argv[2]);
    $res3 = trim($argv[3]);
    if (is_numeric($res1) && is_numeric($res3) && trim($res2) == '+')
        $res = $res1 + $res3;
    else if (is_numeric($res1) && is_numeric($res3) && trim($res2) == '-')
        $res = $res1 - $res3;
    else if (is_numeric($res1) && is_numeric($res3) && trim($res2) == '*')
        $res = $res1 * $res3;
    else if (is_numeric($res1) && is_numeric($res3) && trim($res2) == '%')
        $res = $res1 % $res3;
    else if (is_numeric($res1) && is_numeric($res3) && trim($res2) == '/')
        $res = $res1 / $res3;
    echo $res."\n";
    }
else
    echo "Incorrect Parameters\n";

?>