#!/usr/bin/php
<?php
if ($argc == 2){
    $res = sscanf($argv[1], " %d %c %d %s");
    if ($res[3])
        exit("Incorrect Parameters\n");
    unset($res[3]);
    foreach($res as $elem){
        if (!$elem){
            echo "Syntax Error\n";
            exit ();
        }
    }
    if ($res[1] == '+')
        $res = $res[0] + $res[2];
    else if ($res[1] == '-')
        $res = $res[0] - $res[2];
    else if ($res[1] == '*')
        $res = $res[0] * $res[2];
    else if ($res[1] == '%' && $res[2] != 0)
        $res = $res[0] % $res[2];
    else if ($res[1] == '/' && $res[2] != 0)
        $res = $res[0] / $res[2];
    echo $res."\n";
}
else
    echo "Incorrect Parameters\n";
?>