#!/usr/bin/php
<?php
if ($argc == 2){
	date_default_timezone_set('Europe/Paris');
	$res = sscanf($argv[1], " %s %s %s %s %s %s");
	if (count($res) != 6){
		exit ("Wrong Format\n");
	}
	for($i = 0; $i < count($res); $i++)
	{
		if ($i == 0){
			if (!preg_match("/([Ll]undi|[Mm]ardi|[Mm]ercredi|[Jj]eudi|[Vv]endredi|[Ss]amedi|[Dd]imanche)/", $res[$i]))
				exit ("Wrong Format\n");
		}
		else if ($i == 1){
			if (!preg_match("/^([0-9]|0[1-9]|[1-9]|[1-2][0-9]|3[0-1])$/", $res[$i]))
				exit ("Wrong Format\n");
		}
		else if ($i == 2){
			if (!preg_match("/^([Jj]anvier|[Ff]evrier|[Mm]ars|[Aa]vril|[Mm]ai|[Jj]uin|[Jj]uillet|[Aa]out|[Ss]eptembre|[Oo]ctobre|[Nn]ovembre|[Dd]ecembre)$/", $res[$i]))
				exit ("Wrong Format\n");
		}
		else if ($i == 3){
			if (!preg_match("/^[0-9][0-9][0-9][0-9]$/", $res[$i]))
				exit ("Wrong Format\n");
		}
		else if ($i == 4){
			if (!preg_match("/^([01][0-9]|[2][0-3]):([0-5][0-9]):([0-5][0-9])$/", $res[$i]))
				exit ("Wrong Format\n");
		}
	}
	$month_en = array("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december");
	$month_fr = array("janvier", "fevrier",  "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre");
	$day_fr = array("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche");
	$day_en = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
	$day = array("monday" => 1, "tuesday" => 2, "wednesday" => 3, "thursday" => 4, "friday" => 5, "saturday" => 6, "sunday" => 7);
	$month = array("january" => 1, "february" => 2, "march" => 3, "april" => 4, "may" => 5, "june" => 6, "july" => 7, "august" => 8, "september" => 9, "october" => 10, "november" => 11, "december" => 12);
	for ($i = 0; $i <= count($res); $i++)
	{
		if ($i == 0)
			$res[$i] = $day_en[array_search(strtolower($res[$i]), $day_fr)];
		if ($i == 2){
			$res[$i] = $month_en[array_search(strtolower($res[$i]), $month_fr)];
		}
	}
	$yy = $res[3];
	$mm = $month[strtolower($res[2])];
	if ($mm < 10)
		$mm = "0".$mm;
	$dd = $res[1];
	if ($dd < 10 && $dd[0] != 0)
		$dd = "0".$dd;
	$tt = $res[4];
	$final = strtotime("$yy:$mm:$dd $tt");
	if (date("N", $final) == $day[strtolower($res[0])])
        echo ($final)."\n";
    else
	    exit ("Wrong Format"."\n");
}
?>