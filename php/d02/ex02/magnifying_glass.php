#!/usr/bin/php
<?php
if ($argc == 2){
    $res = file_get_contents($argv[1]);
    $res = preg_replace_callback("/<a.*\/a>/", function($matches){
        $matches[0] = preg_replace_callback("/(title=\"(.*)\")/", function($math){
            return (str_replace($math[2],strtoupper($math[2]),$math[0]));
        }, $matches[0]);
        $matches[0] = preg_replace_callback("/>(.*?)</", function($math){
            return(str_replace($math[1], strtoupper($math[1]),$math[0]));
        }, $matches[0]);
        return ($matches[0]);
    }, $res);
}
echo $res;
?>