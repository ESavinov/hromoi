#!/usr/bin/php
<?php
date_default_timezone_set('Europe/Moscow');
$res = fopen("/var/run/utmpx", "r");
$str = fread($res, 628);
$i = 0;
while ($str = fread($res, 628))
{
    $array = unpack("a256name/a4terminal/a32tty/i1pid/i1login/i1timestamp", $str);
    if ($array["login"] == 7)
    {
        $name[$i] = $array["name"];
        $tty[$i] = $array["tty"];
        $time[$i] = $array["timestamp"];
        $time_int[$i] = date('F  j H:i', $array["timestamp"]);
        $i++;
    }
}

if ($array[0] !== "")
{
    array_multisort($time, $name, $tty, $time_int);
    for($j = 0; $j < count($name); $j++)
    {
        $result = $name[$j]." ".$tty[$j]."  ".$time_int[$j];
        echo $result."\n";
    }
}
?>