<?php
	if ($_POST['submit'] == "OK" && $_POST['login'] && $_POST['passwd'] && $_POST['submit'])
	{
		if (!file_exists("./private/passwd")){
			mkdir("./private", 0777);
			file_put_contents("./private/passwd", "");
		}
		else{
			$arr = unserialize(file_get_contents("private/passwd"));
			foreach($arr as $elem)
			{
				if ($elem['login'] == $_POST['login'])
					exit("ERROR\n");
			}
		}
		$arr[] = array('login' => $_POST['login'], 'passwd' => hash("md5", $_POST['passwd']));
		file_put_contents("./private/passwd", serialize($arr));
		echo "OK\n";
	}
	else
		exit("ERROR\n");
?>
