#include "printf.h"

void	*ft_memset(void *destination, int c, size_t n)
{
	char *dest;

	dest = (char *)destination;
	while (n--)
		*dest++ = (char)c;
	return (destination);
}

int ft_print_str(char *str, t_flags *flag)
{
	if (!str)
	{
		write(1, "(null)", 6);
		return (6);
	}
	flag->len = strlen(str);
	if (flag->tochn && *str)
		flag->len = atoi(flag->toch);
	if (flag->width_acc)
		flag->lenstr = atoi(flag->width_accur) - flag->len;
	if (flag->width_acc && !flag->minus)
		write(1, flag->probel, flag->lenstr);
	write(1, str, flag->len);
	if (flag->width_acc && flag->minus)
		write(1, flag->probel, flag->lenstr);
	return (flag->width_acc && atoi(flag->width_accur) > flag->len ? atoi(flag->width_accur) : flag->len);
}

int ft_print_add(t_struct *tmp, t_flags *flag)
{
	flag->len = atoi(flag->width_accur) - strlen(tmp->temp) - 2;
	if (flag->width_acc && !flag->minus)
		write(1, flag->probel, flag->len);
	write(1, "0x", 2);
	write(1, tmp->temp, strlen(tmp->temp));
	if (flag->width_acc && flag->minus)
		write(1, flag->probel, flag->len);
	return (0);
}

int ft_char(char c, t_flags *flag)
{
	flag->len = atoi(flag->width_accur) - 1;
	if (flag->width_acc && !flag->minus)
		write(1, flag->probel, flag->len);
	write(1, &c, 1);
	if (flag->width_acc && flag->minus)
		write(1, flag->probel, flag->len);
	return (flag->width_acc ? flag->len + 1 : 1);
}

int ft_len(long long int hex, int base, t_flags *flag)
{
	int i = 0;

	while (hex)
	{
		hex /= base;
		i++;
	}
	return (i);
}

int ft_print_int(t_struct *str, t_flags *flag)
{
	flag->lenstr = strlen(str->result);
	flag->len_width = atoi(flag->width_accur);
	flag->len_toch = atoi(flag->toch);

	if (flag->width_acc || flag->tochn)
	{
		if (!flag->minus)
		{
			if (!flag->zero)
			{
				if (!str->sign)
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							flag->plus ? write (1, flag->probel, --flag->len_width - flag->lenstr) : write (1, flag->probel, flag->len_width - flag->lenstr);
							if (flag->plus)
								write(1, "+", 1);
						}
						else
						{
							if (flag->plus)
								flag->len_width--;
							if (flag->len_width > flag->len_toch)
								write (1, flag->probel, flag->len_width - flag->len_toch);
							if (flag->plus)
								write(1, "+", 1);
							if (flag->len_toch > flag->lenstr)
								write (1, flag->null, flag->len_toch - flag->lenstr);
						}
					}
					else
					{
						if (flag->len_toch > flag->lenstr)
							write (1, flag->null, flag->len_toch - flag->lenstr);
					}
				}
				else
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
							write (1, flag->probel, flag->len_width - flag->lenstr);
						else
						{
							flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
							if (flag->len_toch >= flag->lenstr)
							{
								write(1, str->result++, 1);
									flag->lenstr--;
								write (1, flag->null, flag->len_toch - flag->lenstr);
							}
						}
					}
					else
					{
						if (flag->len_toch > flag->lenstr)
						{
							write(1, str->result++, 1);
							flag->lenstr--;
							write (1, flag->null, flag->len_toch - strlen(str->result));
						}
					}
				}
				write(1, str->result, flag->lenstr);
			}
			else
			{
				if (!str->sign)
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							if (flag->plus)
							{
								write(1, "+", 1);
								flag->len_width--;
							}
							write(1, flag->null, flag->len_width - flag->lenstr);
						}
						else
						{
							if (flag->plus)
								flag->len_width--;
							if (flag->len_width > flag->len_toch)
								write(1, flag->probel, flag->len_width - flag->len_toch);
							if (flag->plus)
								write(1, "+", 1);
							write(1, flag->null, flag->len_toch - flag->lenstr);
						}
						write(1, str->result, flag->lenstr);
					}
					else
					{
						if (flag->plus)
							flag->len_width--;
						if (flag->len_width > flag->len_toch)
							write(1, flag->probel, flag->len_width - flag->len_toch);
						if (flag->plus)
							write(1, "+", 1);
						write(1, flag->null, flag->len_toch - flag->lenstr);
						write(1, str->result, flag->lenstr);
					}
				}
				else
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							write(1, str->result++, 1);
							write(1, flag->null, flag->len_width - flag->lenstr--);
						}
						else
						{
							if (flag->len_toch >= flag->lenstr)
							{
								write(1, str->result++, 1);
								write(1, flag->null, flag->len_toch - --flag->lenstr);
							}
							else if (flag->len_width > flag->len_toch)
								write(1, flag->probel, --flag->len_width - flag->len_toch);
						}
						write(1, str->result, flag->lenstr);
					}
					else
					{
						write(1, str->result++, 1);
						write(1, flag->null, flag->len_toch - --flag->lenstr);
						write(1, str->result, flag->lenstr);
					}
				}
			}
		}
		else
		{
			if (!flag->zero)
			{
				if (!str->sign)
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							if (flag->plus)
								write(1, "+", 1);
							write(1, str->result, flag->lenstr);
							flag->plus ? write (1, flag->probel, --flag->len_width - flag->lenstr) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
						else
						{
							if (flag->plus)
							{
								write(1, "+", 1);
								flag->len_width--;
							}
							if (flag->len_toch > flag->lenstr)
								write (1, flag->null, flag->len_toch - flag->lenstr);
							write(1, str->result, flag->lenstr);
							flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
					}
					else
					{
						if (flag->plus)
						{
							write(1, "+", 1);
							flag->len_width--;
						}
						if (flag->len_toch > flag->lenstr)
							write (1, flag->null, flag->len_toch - flag->lenstr);
						write(1, str->result, flag->lenstr);
						flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
					}
				}
				else
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							write(1, str->result, flag->lenstr);
							flag->plus ? write (1, flag->probel, flag->len_width - flag->lenstr) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
						else
						{
							if (flag->len_toch >= flag->lenstr)
							{
								write(1, str->result++, 1);
								flag->lenstr--;
								flag->len_width--;
								write (1, flag->null, flag->len_toch - strlen(str->result));
							}
							write(1, str->result, flag->lenstr);
							flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
					}
					else
					{
						if (flag->len_toch >= flag->lenstr)
						{
							write(1, str->result++, 1);
							flag->lenstr--;
							flag->len_width--;
							write (1, flag->null, flag->len_toch - strlen(str->result));
						}
						write(1, str->result, flag->lenstr);
						flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
					}
				}
			}
			else
			{
				if (!str->sign)
				{
					if (flag->width_acc)
					{
						if (!flag->tochn)
						{
							if (flag->plus)
								write(1, "+", 1);
							write(1, str->result, flag->lenstr);
							flag->plus ? write (1, flag->probel, --flag->len_width - flag->lenstr) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
						else
						{
							if (flag->plus)
							{
								write(1, "+", 1);
								flag->len_width--;
							}
							if (flag->len_toch > flag->lenstr)
								write (1, flag->null, flag->len_toch - flag->lenstr);
							write(1, str->result, flag->lenstr);
							flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
						}
					}
					else
					{
						if (flag->plus)
						{
							write(1, "+", 1);
							flag->len_width--;
						}
						if (flag->len_toch > flag->lenstr)
							write (1, flag->null, flag->len_toch - flag->lenstr);
						write(1, str->result, flag->lenstr);
						flag->len_toch >= flag->lenstr ? write (1, flag->probel, flag->len_width - flag->len_toch) : write (1, flag->probel, flag->len_width - flag->lenstr);
					}
				}
				else
				{
					if (!flag->tochn)
					{
						write(1, str->result, flag->lenstr);
						write (1, flag->probel, flag->len_width - flag->lenstr);
					}
					else
					{
						if (flag->lenstr <= flag->len_toch)
						{
							write(1, str->result++, 1);
							if (flag->len_toch > --flag->lenstr)
								write (1, flag->null, flag->len_toch - flag->lenstr);
							write(1, str->result, flag->lenstr);
							write (1, flag->probel, flag->len_width - (flag->len_toch + 1));
						}
						else
						{
							write(1, str->result, flag->lenstr);
							if (flag->len_width > flag->lenstr)
								write (1, flag->probel, flag->len_width - flag->lenstr);
						}
					}
				}
			}
		}
	}
	else
	{
		if (!flag->plus && flag->space && !str->sign)
		{
			write(1, " ", 1);
			flag->lenstr++;
		}
		else if (flag->plus && !str->sign)
		{
			write(1, "+", 1);
			flag->lenstr++;
		}
		write(1, str->result, strlen(str->result));
	}
	return (flag->lenstr);
}

int ft_itoa_base(long long int num, int base, t_flags *flag)
{
	t_struct	tmp = { 0,0,0 };

	tmp.temp = "0123456789abcdef";
	if (num < 0)
	{
		tmp.sign = 1;
		tmp.res = num * -1;
	}
	else if (!num)
		tmp.len = 1;
	else
		tmp.res = num;
	if (num)
		tmp.len = ft_len(num, base, flag);
	tmp.result = (char *)malloc(sizeof(char) * (tmp.len + tmp.sign + 1));
	if (tmp.sign == 1)
		tmp.result[0] = '-';
	tmp.result[tmp.len + tmp.sign] = '\0';
	while (--tmp.len >= 0)
	{
		tmp.result[tmp.sign + tmp.len] = tmp.temp[tmp.res % base];
		tmp.res /= base;
	}
	if (flag->p)
		return (ft_print_add(&tmp, flag));
	else if (flag->d)
		return (ft_print_int(&tmp, flag));
	return (0);
}

int check_razmer(long long int num, t_flags *flags)
{
	if (flags->d)
	{
		if (flags->l)
			num = (long int)num;
		else if (flags->ll)
			num = (long long int)num;
		else if (flags->hh)
			num = (signed char)num;
		else if (flags->h)
			num = (short int)num;
		else if (flags->j)
			num = (intmax_t)num;
		else if (flags->z)
			num = (ssize_t)num;
		else
			num = (int)num;
	}
	return (ft_itoa_base(num, 10, flags));
}

int ft_address(void *temp, t_flags *flag)
{
	unsigned long long int tmp = (unsigned long long int)temp;
	return (ft_itoa_base(tmp, 16, flag));
}

void ft_unsign_int(unsigned long long int temp, t_flags *flag)
{
	unsigned int k = (unsigned int)temp;

	if (k > 9)
	{
		ft_unsign_int(k / 10, flag);
		ft_unsign_int(k % 10, flag);
	}
	if (k <= 9)
	{
		char f = k + '0';
		write(1, &f, 1);
	}
}

char *ft_check_format(char *format, t_flags *flags)
{
	int i = 0;

	while(*format && *format != '%')
	{
		while (*format && (*format == '#' || *format == ' ' || *format == '0' || *format == '-' || *format == '+'))
		{
			if (*format == '#')
				flags->resh = 1;
			else if (*format == ' ')
				flags->space = 1;
			else if (*format == '0')
				flags->zero = 1;
			else if (*format == '-')
				flags->minus = 1;
			else if (*format == '+')
				flags->plus = 1;
			format++;
		}
		while (*format >= 48 && *format <= 57)
		{
			flags->width_accur[i++] = *format++;
			flags->width_acc = 1;
		}
		flags->width_accur[i] = '\0';
		if (*format == '.')
		{
			format++;
			i = 0;
			while (*format >= 48 && *format <= 57)
			{
				flags->toch[i++] = *format++;
				flags->tochn = 1;
			}
			flags->toch[i] = '\0';
		}
		if (*format == 'h' || (*format == 'h' && *(format + 1) == 'h') || *format == 'l' || (*format == 'l' && *(format + 1) == 'l') || *format == 'j' || *format == 'z')
		{
			if (*format == 'h' && *(format + 1) == 'h')
			{
				flags->hh = 1;
				format += 2;
			}
			else if (*format == 'h')
			{
				flags->h = 1;
				format++;
			}
			else if (*format == 'l' && *(format + 1) == 'l')
			{
				flags->ll = 1;
				format += 2;
			}
			else if (*format == 'l')
			{
				flags->l = 1;
				format++;
			}
			else if (*format == 'j')
			{
				flags->j = 1;
				format++;
			}
			else if (*format == 'z')
			{
				flags->z = 1;
				format++;
			}
		}
		if (*format == 'd' || *format == 'i' || *format == 'o' || *format == 'u' || *format == 'x' || *format == 'X' || *format == 'f')
		{
			if (*format == 's')
				flags->s = 1;
			else if (*format == 'p')
				flags->p = 1;
			else if (*format == 'd' || *format == 'i')
				flags->d = 1;
			else if (*format == 'o')
				flags->o = 1;
			else if (*format == 'u')
				flags->u = 1;
			else if (*format == 'x')
				flags->x = 1;
			else if (*format == 'X')
				flags->X = 1;
			else if (*format == 'f')
				flags->f = 1;
		}
		else if (*format != 'd' && *format != 'i' && *format != 'o' && *format != 'u' && *format != 'x' && *format != 'X' && *format != 'f' \
		&& *format != 'h' && *(format + 1) != 'h' && *format != 'l' && *(format + 1) != 'l' && *format != '#' && *format != ' ' && *format != '0' \
		&& *format != '-' && *format != '+')
			flags->other = *format;
		break;
	}
	return (format);
}

void ft_null(t_flags *flags)
{
	flags->resh = 0;
	flags->space = 0;
	flags->plus = 0;
	flags->minus = 0;
	flags->zero = 0;
	flags->h = 0;
	flags->hh = 0;
	flags->l = 0;
	flags->ll = 0;
	flags->j = 0;
	flags->z = 0;
	// flags->coll = 0;
	flags->tochn = 0;
	flags->width_acc = 0;
	// flags->len = 0;
	// flags->lenstr = 0;
	// flags->len_width = 0;
	// flags->len_toch = 0;
	flags->s = 0;
	flags->p = 0;
	flags->d = 0;
	flags->o = 0;
	flags->u = 0;
	flags->x = 0;
	flags->X = 0;
	flags->f = 0;
}

int ft_printf(char *format, ...)
{
	va_list var; //задаем переменную типа va_list
	t_flags flags = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	ft_memset(flags.width_accur, '\0', 1000);
	ft_memset(flags.toch, '\0', 1000);
	ft_memset(flags.probel, ' ', 1000);
	ft_memset(flags.null, 48, 1000);

	va_start(var, format); //устанавливаем указатель var на первый неявный аргумент
	while(*format)
	{
		if (*format != '%')
		{
			write (1, &(*format), 1);
			flags.coll++;
		}
		else if (*format == '%' && *(format + 1) == '%')
		{
			write (1, &(*format), 1);
			format += 1;
			flags.coll++;
		}
		else
		{
			format += 1;
			format = ft_check_format(format, &flags);
			// printf("*format - %c\n", *format);
			if (*format == 's')
				flags.coll += ft_print_str(va_arg(var, char *), &flags);
			else if (*format == 'c')
				flags.coll += ft_char((char)va_arg(var, long long int), &flags);
			else if (*format == 'p')
				flags.coll += ft_address(va_arg(var, void *), &flags);
			else if (*format == 'd' || *format == 'i')
				flags.coll += check_razmer(va_arg(var, long long int), &flags);
			// else if (*format == 'o')
			// 	ft_itoa_base(va_arg(var, long long int), 8, &flags);
			// else if (*format == 'u')
			// 	ft_unsign_int(va_arg(var, unsigned long long int), &flags);
			// else if (*format == 'x')
			// 	ft_itoa_base(va_arg(var, long long int), 16, &flags);
			// else if (*format == 'X')
			// 	ft_itoa_base(va_arg(var, long long int), 16, &flags);
			// else if (*format == 'f')
			// 	ft_itoa_base(va_arg(var, long long int), &flags);
			else
				flags.coll += ft_char(flags.other, &flags);			
			ft_null(&flags);
		}
		format++;
	}
	va_end(var);
	return (flags.coll);
}