#ifndef PRINTF_H
#define PRINTF_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

typedef struct              s_struct
{
    int						len;
	int						sign;
	unsigned long long int	res;
	char					*temp;
	char					*result;

}                           t_struct;

typedef struct 				s_flags
{
	int						resh;
	int						space;
	int						plus;
	int						minus;
	int						zero;
	int						h;
	int						hh;
	int 					l;
	int						ll;
	int						j;
	int						z;
	int						s;
	int						p;
	int						d;
	int						o;
	int						u;
	int						x;
	int						X;
	int						f;
	int						coll;
	int						width_acc;
	int						tochn;
	char					other;
	int 					len;
	int						lenstr;
	int						len_width;
	int						len_toch;
	char					width_accur[1000];
	char					toch[1000];
	char					probel[1000];
	char					null[1000];
}							t_flags;

int						ft_printf(char *format, ...);

#endif