int ft_print_int(t_struct *str, t_flags *flag)
{
	int lenstr = (int)strlen(str->result);
	flag->len = atoi(flag->width_accur);
	int len = flag->len - lenstr;
	int tochnost = atoi(flag->toch);

	if (flag->width_acc)
	{
		if (str->sign == 0 && flag->plus && flag->zero)
			write(1, "+", 1);
		if ((str->sign > 0 && flag->zero && flag->width_acc) || (str->sign > 0 && flag->plus && !flag->width_acc))
		{
			write(1, str->result++, 1);
			lenstr--;
		}
		if ((!atoi(str->result) && flag->toch[0] == 48 && flag->toch[1] == '\0') || (!atoi(str->result) && flag->toch[0] == '\0' && flag->toch[1] == '\0'))
		{
			while (flag->len--)
				write(1, " ", 1);
			return(atoi(flag->width_accur));
		}
		if (!flag->minus && !flag->zero && !flag->tochn)
		{
			if ((flag->plus && !flag->width_acc) || (flag->plus && flag->space && flag->width_acc && str->sign == 0) || (str->sign == 0 && flag->plus && flag->width_acc))
				len--;
			while(len-- && !flag->tochn)
				write(1, " ", 1);
		}
		else if (!flag->minus && flag->zero && !flag->tochn)
		{
			if (flag->space && flag->zero)
			{
				if (!flag->plus)
					write(1, " ", 1);
				if (str->sign == 0)
					len--;
			}
			if (!flag->space && flag->zero && flag->plus && flag->width_acc && str->sign == 0)
				len--;
			while (len--)
				write(1, "0", 1);
		}
		if (flag->tochn && tochnost >= lenstr)
		{
			int temp = tochnost - lenstr;
			if (str->sign > 0 && tochnost == lenstr)
				temp = 1;
			if (flag->width_acc && flag->len > tochnost && !flag->minus)
			{
				if (flag->plus)
					tochnost++;
				while (flag->len - tochnost++ && str->sign == 0)
					write(1, " ", 1);
				if (flag->plus && str->sign == 0)
					write(1, "+", 1);
				if (flag->plus && str->sign > 0)
					write(1, str->result++, 1);
				tochnost--;
			}
			else if (flag->width_acc && flag->len > tochnost && flag->minus)
			{
				if (flag->plus && str->sign == 0)
					write(1, "+", 1);
				if (flag->plus && str->sign > 0)
					write(1, str->result++, 1);
				if (flag->plus)
					tochnost++;
			}
			while (temp--)
				write(1, "0", 1);
			write(1, str->result, lenstr);
			if (flag->width_acc && flag->len > tochnost && flag->minus)
			{
				while (flag->len - tochnost++)
					write(1, " ", 1);
				tochnost--;
			}
			return (flag->len > tochnost ? flag->len : tochnost);
		}
		else if (flag->minus)
		{
			write(1, str->result, lenstr);
			while(len--)
				write(1, " ", 1);
			return (flag->len);
		}
		if (str->sign == 0 && flag->plus && !flag->zero)
			write(1, "+", 1);
		write(1, str->result, lenstr);
	}
	else
	{
		if (flag->space && !flag->plus && str->sign == 0)
		{
			write(1, " ", 1);
			write(1, str->result, lenstr);
			return (strlen(str->result) + 1);
		}
		else if (flag->plus && str->sign == 0)
		{
			write(1, "+", 1);
			write(1, str->result, lenstr);
			return (strlen(str->result) + 1);
		}
		if (flag->tochn && tochnost > lenstr)
		{
			if (str->sign > 0)
			{
				write(1, str->result++, 1);
				lenstr--;
			}
			tochnost = tochnost - lenstr;
			int temp = tochnost;
			while (temp--)
				write(1, "0", 1);
			write(1, str->result, lenstr);
			if (str->sign > 0)
				lenstr++;
			return (tochnost + lenstr);
		}
		if (atoll(str->result) || flag->z || flag->j)
			write(1, str->result, lenstr);
		else
			return (0);
	}
	return (flag->width_acc ? flag->len : lenstr);
}