/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 12:11:04 by yperra-f          #+#    #+#             */
/*   Updated: 2018/11/28 14:42:58 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstring(char **av, int ac)
{
	int i;
	int j;

	j = 0;
	i = 1;
	while (i != ac)
	{
		while (av[i][j] != '\0')
		{
			ft_putchar(av[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
		j = 0;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1 != '\0')
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

void	ft_swap(char **av, char **av1)
{
	char *c;

	c = *av;
	*av = *av1;
	*av1 = c;
}

int		main(int ac, char **av)
{
	int i;
	int k;

	k = ac - 1;
	i = 1;
	if (ac > 1)
	{
		while (k > 0)
		{
			while (i < k)
			{
				if (ft_strcmp(av[i], av[i + 1]) > 0 && *av && *(av + 1))
				{
					ft_swap(&av[i], &av[i + 1]);
				}
				i++;
			}
			k = k - 1;
			i = 0;
		}
		ft_putstring(av, ac);
	}
	return (0);
}
