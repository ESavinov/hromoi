/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yperra-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 10:57:43 by yperra-f          #+#    #+#             */
/*   Updated: 2018/11/26 13:23:32 by yperra-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int i;
	int *c;

	c = NULL;
	i = 0;
	if (min >= max)
		return (NULL);
	while (min <= max)
	{
		i++;
		min++;
	}
	min = min - i;
	c = (int *)malloc((i - 1) * sizeof(int));
	i = 0;
	while (min < max)
	{
		c[i] = min;
		min++;
		i++;
	}
	return (c);
}
