#include <stdio.h>
#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void ft_putstring(char **str, int ac)
{
	int i;
	int j;

	i = 1;
	j = 0;
	while(i != ac)
		{
			while(str[i][j] != '\0')
				{
					ft_putchar(str[i][j]);
					j++;
				}
			i++;
			ft_putchar('\n');
			j = 0;
		}
}

int ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i])
			i++;
	return (s1[i] - s2[i]);
}

void ft_swap(char **av, char **av1)
{
	char *c;

	c = *av;
	*av = *av1;
	*av1 = c;	
}

int main(int ac, char **av)
{
	int i;
	int j;

	j = ac - 1;
	i = 1;
	while (j > 0)
		{
		while (i < j)
			{
				if (ft_strcmp(av[i], av[i + 1]) > 0)
					{
						printf("%p\n%p\n%d", &av[i], av[i], *av[i]);
						ft_swap(&av[i], &av[i + 1]);
						i++;
					}
				else
					i++;	
			}
		j--;
		i = 1;
		}
	ft_putstring(av, ac);
	return (0);
}
