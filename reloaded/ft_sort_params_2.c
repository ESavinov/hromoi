#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void ft_putstring(char **av, int ac)
{
	int i;
	int j;

	i = 1;
	j = 0;
	while (i != ac)
	{
		while(av[i][j] != '\0')
			{
				ft_putchar(av[i][j]);
				j++;
			}
		ft_putchar('\n');
		j = 0;
		i++;
	}
}

void ft_swap(char **av, char **av1)
{
	char *c;

	c = *av;
	*av = *av1;
	*av1 = c;
}

int ft_strcmp(char *av, char *av1)
{
	int i;

	i = 0;
	while (av[i] == av1[i])
			i++;
	return (av[i] - av1[i]);
}

int main(int ac, char **av)
{
	int i;
	int k;

	i = 1;
	k = ac - 1;
	while (i != k)
		{
			if (ft_strcmp(av[i], av[i + 1]) > 0)
				{
					ft_swap(&av[i], &av[i + 1]);
					i = 1;		
				}
			else
				i++;
		}
	ft_putstring(av, ac);
	return (0);
}
