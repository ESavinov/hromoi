#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 200000
#define MIN 1

int ft_strlen(char *test)
{
    int i;

    i = 0;
    while(*test)
    {
        i++;
        test++;
    }
    return (i);
}

char *ft_strdup(char *test)
{
	char *copy;
	char *temp;
    int i;

	i = ft_strlen(test);
	if (!(copy = (char *)malloc((i + 1) * sizeof(char))))
		return (NULL);
	temp = copy;
	while (*test)
	{
		*copy = *test;
		copy++;
		test++;
	}
	*copy = '\0';
	return (temp);
}

int main()
{
    char    *test;
    char    *search;
    char    *temp;
    int     len;
    int     i;
    int     x;

	printf("Введите длину строки - ");
    x = scanf("%d", &len);
    if (len < MIN || len > MAX)
        return (-1);
	printf("Введите строку - ");
    x = scanf("%s", test);
    x = 0;
    i = len - 1;
    while (i != -1)
    {
        if (search)
            free (search);
        if (!(search = ft_strdup(test)))
        	return (-1);
        search[len - i] = '\0';
        temp = test;
        while (*temp)
        {
            if (!(temp = strstr(temp, search)))
            {
                break ;
                return (x);
            }
            else
            {
                x++;
                temp += 1;
            }
        }
        i--;
    }
    return (x);
}