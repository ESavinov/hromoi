#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

char *malloc_word(char *str)
{
	int i = 0;
	char *res;

	while (str[i] != ' ' && str[i] != '\t')
		i++;
	res = (char *)malloc(sizeof(char) * i + 1);
	i = 0;
	while (str[i] && str[i] != ' ' && str[i] != '\t')
	{
		res[i] = str[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}

char **ft_split(char *str, unsigned long long int coll)
{
	int	i = 0;
	char	**res = (char **)malloc(sizeof(char *) * coll + 1);
	while (*str)
	{
		while (*str && (*str == ' ' || *str == '\t'))
			str++;
		if (*str)
			res[i++] = malloc_word(str);
		while (*str && *str != ' ' && *str != '\t')
			str++;
	}
	res[i] = NULL;
	return (res);
}

void ft_reshenie(char **stri, int iter, unsigned long long int coll)
{
	unsigned long long int	itog = 0;
	srand(time(NULL));

	while (iter)
	{
		int k = rand() % coll;
		printf("index - %d\n", k);
		itog += atoll(stri[k]);
		iter--;
	}
	printf("%llu\n", itog);
}

int main()
{
	unsigned long long int	coll;
	int						iter;
	char					str[1000000];
	char					**stri;

	scanf("%lld\n", &coll);
	srand(time(NULL));
	scanf("%[^\n]", str);
	stri = ft_split(str, coll);
	iter = 1 + rand() % coll;
	printf("iter - %d\n", iter);
	ft_reshenie(stri, iter, coll);
}