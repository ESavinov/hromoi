#include <stdio.h>
#include <stdlib.h>

void ft_len(char *av)
{
	unsigned long long int len = 0;

	while (*av)
	{
    		if (*av > 64 && *av < 91 && *(av + 1) > 47 && *(av + 1) < 58)
    		{
    			av++;
    			len += atoll(av);
        		while (*av && *av > 47 && *av < 58)
            			av++;
    		}
    		else
        	{
        		len++;
        		av++;
        	}   	
    	}
	printf("%llu\n", len);
}

int main(void)
{
	char *vvod;
	int k = scanf("%s", vvod);
	ft_len(vvod);
	return (0);
}
