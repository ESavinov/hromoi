#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#define MAX_SIZE 2000000000
#define MIN_SIZE -2000000000 

int ft_strlen(int c)
{
	int i;
	c > 0 ? (i = 0) : (i = 1);

	while (c != 0)
	{
		c = c / 10;
		i++;
	}
	return (i);
}

char *ft_itoa(int c, int len)
{
	char *s;
	int i = 0;
	int flag;
	int k;
	char t;

	if ((s = (char *)malloc((len + 1) * sizeof(char))) == NULL)
		return (NULL);
	if (c < 0)
	{
		c *= -1; 
		s[--len] = '-';
		len++;
	}
	while (c > 9)
	{
		s[i++] = c % 10 + '0';
		c = c / 10;
	}
	s[i] = c + '0';
	s[len] = '\0';
	i = 0;
	len % 2 == 1 ? (k = len - 1) : (k = len);
	while (i < k)
	{
		t = s[i];
		s[i] = s[len - i - 1];
		s[len - i - 1] = t;
		i++;
		k--;
	}
	return (s);
}

int main()
{
	char buff[50];
	char *temp;
	int a;
	int b;
	int fd = open("input.txt", O_RDONLY);
	int fd2 = open("output.txt", O_RDWR | O_CREAT, 0777);
	if (fd < 0 || fd2 < 0)
		return (-1);
	if ((read(fd, buff, 50)) < 0)
		return (-1);
	temp = strchr(buff, ' ');
	a = atoi(temp + 1);
	b = atoi(buff);
	if (a > MAX_SIZE || b > MAX_SIZE || a < MIN_SIZE || b < MIN_SIZE)
		return (-1);
	int c = a + b;
	int len = ft_strlen(c);
	if ((temp = ft_itoa(c, len)) == NULL)
		return (-1);
	a = write(fd2, temp, len);
	free(temp);
	close(fd);
	close(fd2);
	return (0);
}
