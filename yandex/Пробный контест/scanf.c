#include <stdio.h>
#define MAX_SIZE 2000000000
#define MIN_SIZE -2000000000 

int main()
{
	int a;
	int b;
	int unused;
	unused = scanf("%d %d", &a, &b);
    if (a > MAX_SIZE || b > MAX_SIZE || a < MIN_SIZE || b < MIN_SIZE)
		return (-1);
	printf("%d", a + b);
	return (0);
}